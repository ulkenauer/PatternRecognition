from PIL import Image, ImageDraw, ImageFont, ImageTransform
from collections import Counter
import heapq

class GlyphSymbolStringException(Exception):
    pass


class GlyphTemplate(object):
    def __init__(self, symbol: str):
        if len(symbol) != 1:
            raise GlyphSymbolStringException()
        self.symbol = symbol
        font = ImageFont.truetype('arial.ttf', 64)
        img = Image.new('1', font.getsize(text=symbol), color=1)
        imgDrawer = ImageDraw.Draw(img)
        imgDrawer.text((0, 0), text=symbol, font=font)
        self.img = img

    def normalize(self):
        box = self.buildBounds()
        self.img = self.img.crop((box['left'],box['up'],box['right']+1,box['down']+1))
        self.img = self.img.resize((32,32))

    def buildBounds(self):
        px = self.img.load()
        size = self.img.size
        box = {'left': 0, 'up': 0,
               'right': size[0]-1, 'down': size[1]-1}
        latches = {'left': False, 'right': False,
                   'up': False, 'down': False}

        #print(box)

        def megaLatch():  # returns true when all latches are true
            result = True
            for latch in latches.items():
                result &= latch[1]
            return result
        cursor = [0, 0]

        directions = {'right': (0, 1), 'left': (
            0, -1), 'up': (1, -1), 'down': (1, 1)}
        shifts = {'right': ('up', 1), 'left': ('down', -1),
                  'up': ('left', 1), 'down': ('right', -1)}

        def step(direction):
            coord = directions[direction][0]
            increment = directions[direction][1]
            shift = shifts[direction][0]
            inc = shifts[direction][1]
            if latches[direction]:
                cursor[coord] = box[direction]
            while cursor[coord] != box[direction]:
                if px[cursor[0], cursor[1]] == 0: #latches[direction] == False and px[cursor[0], cursor[1]] == 0:
                    latches[direction] = True
                cursor[coord] += increment
            if px[cursor[0], cursor[1]] == 0: #latches[direction] == False and px[cursor[0], cursor[1]] == 0:
                latches[direction] = True
            if latches[direction] == False:
                box[shift] += inc

        while megaLatch() != True:
                step('right')
                step('down')
                step('left')
                step('up')
        #print(box)
        return box

class Glyph(GlyphTemplate):
    def __init__(self, img: Image, templates: list):
        self.img = img
        self.symbol = ''
    def recognize(self):
        result = []
        for template in templates:
            cnt = 0
            px = self.img.load()
            tpx = template.img.load()
            for x in range(32):
                for y in range(32):
                    if(px[x,y] == tpx[x,y]):
                        cnt+=1
                    else:
                        cnt-=1
            heapq.heappush(result, (cnt,template))
        self.symbol = heapq.nlargest(1,result)[0][1].symbol
        largests = heapq.nlargest(3,result)
        for largest in largests:
            print('symbol =' + largest[1].symbol + ' value = ' + str(largest[0]))

templates = [GlyphTemplate('a'),GlyphTemplate('k'),GlyphTemplate('s')]
var = 1
for template in templates:
    template.normalize()
    template.img.save('template' + str(var) + '.png')
    var +=1

images = []
glyphes = []
'''
images.append(Image.open('K1.png'))
images.append(Image.open('K2.png'))
images.append(Image.open('K3.png'))
images.append(Image.open('K4.png'))
'''

images.append(Image.open('A1.png'))
images.append(Image.open('A2.png'))
images.append(Image.open('A3.png'))

'''
images.append(Image.open('S1.png'))
images.append(Image.open('S2.png'))
images.append(Image.open('S3.png'))
'''

for image in images:
    glyphes.append(Glyph(image, templates))

var = 1
for glyph in glyphes:
    glyph.normalize()
    glyph.recognize()
    print(glyph.symbol)
    glyph.img.save('y' + str(var) + '.png')
    var+=1

'''
img = Image.open('glyph2.png')
glyph = Glyph(img, templates)
glyph.normalize()
glyph.recognize()
print(glyph.symbol)
'''
#glyph2.img.save('custom.png')

'''
heap = []
heapq.heappush(heap, (0,'zero'))
heapq.heappush(heap, (0,'two'))
print(heapq.nsmallest(2,heap)) 
'''

text = 'a'
font = ImageFont.truetype('arial.ttf', 14)

print(type(text[0]))


print(font.getsize('g'))

img = Image.new('1', font.getsize(text=text), color=1)

#img = Image.new('1', font.getsize(text=text), color)
imgDrawer = ImageDraw.Draw(img)
#myfile = open('./GOST_A_.ttf','rb+')
#font = ImageFont.truetype(myfile.read(), 25)

imgDrawer.text((0, 0), text=text, font=font)
#imgDrawer.text((0, 0), text=text, font=font, fill=(0,0,0))
#img = img.resize((100,100))
#img = img.transform((100,100), method=Image.QUAD, data=(100,100))
#img = img.transform((100,100), Image.EXTENT, (5,5,img.size[0],img.size[1]), Image.BILINEAR)
#img = img.transform((100,100), ImageTransform.ExtentTransform((5,5,img.size[0],img.size[1])), Image.BILINEAR)

#img2 = Image.new('1', (100,100), color=1)
#img2.paste(img)
#img = img.resize((32,32))
px = img.load()
print(type(px))
#img.save("img.png")
#img2.show()
